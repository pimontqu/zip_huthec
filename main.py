import zipfile
import csv
import os

#path_pdfs = '/home/quentinp/Documents/projets/publipostage/appPHP/appR/assets/rapport/rapportEtudiant/'
path_pdfs = '/home/unisciel/Desktop/PDF générés/rapportEtudiant/'
csv_hutech = 'A 2020 HuTech 01.csv'
#csv_hutech = 'test_list.csv'
list_pdf_hutech = []
hutech_zip = zipfile.ZipFile('hutech_bac.zip', 'w', zipfile.ZIP_DEFLATED)

with open(csv_hutech) as file_hutech:
    hutech_list = csv.reader(file_hutech, delimiter=',')
    i_row = 0
    for student in hutech_list:
        if(i_row == 0):
            firstname_index = student.index('Prénom')
            lastname_index = student.index('Nom')
        else:
            name_pdf = student[firstname_index] + '_' + student[lastname_index] + '.pdf'
            list_pdf_hutech.append(name_pdf)
        i_row += 1

with os.scandir(path_pdfs) as pdfs_dir:
    for pdf in pdfs_dir:
        if(pdf.is_file() and pdf.name in list_pdf_hutech):
            print(pdf.path)
            hutech_zip.write(pdf.path, pdf.name)